<script type="text/javascript" src="templates/js/jquery.min.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmCreateUser').validate(
                {
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },
                        weight: {
                            required: true
                        }
                    },
                    messages: {
                        name: "(*)",
                        weight: "(*)"
                    }
                });
    });
</script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Cập nhật trạm</div>

            <div class="widget-body">
                <form class="form-horizontal no-margin" method="post" action="station.php?actionUpdate=updateStation"
                      id="frmCreate"
                      name="frmCreateUser">
                    {foreach from=$getStationId item="item"}
                    <div class="control-group">
                        <input type="hidden" name="stationId" value="{$item.Station_ID}">
                    </div>
                    <div class="control-group">
                        <label class="control-label">Vị trí</label>

                        <div class="controls">
                            <select name="location">
                                {foreach from=$listLocation item='i'}
                                    {if $i.Location_ID eq $item.Location_ID}
                                        <option value="{$i.Location_ID}" selected="selected">{$i.Name}</option>
                                    {else}
                                        <option value="{$i.Location_ID}">{$i.Name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Tên *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name" name="name" class="required form-control"
                                   value="{$item.Name}"
                                   maxlength="199" placeholder="Nhập địa chỉ tên">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Weight *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="weight" name="weight" class="required form-control"
                                   maxlength="199" placeholder="Nhập weight" value="{$item.Weight}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Lat *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="lat" name="lat" class="required form-control"
                                   maxlength="199" placeholder="Nhập lat" value="{$item.Lat}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Long *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="long" name="long" class="required  form-control"
                                   maxlength="199" placeholder="Nhập long" value="{$item.Longs}">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Owner *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="owner" name="owner" class="required form-control"
                                   maxlength="199" placeholder="Nhập Owner" value="{$item.Owner}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Mô tả *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="description" name="description"
                                   class="required form-control"
                                   maxlength="199" placeholder="Nhập Description" value="{$item.Description}">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-info pull-right" name="btnCreate">Thêm</button>
                    </div>
                    {/foreach}
                </form>
            </div>

    </div>
</div>
