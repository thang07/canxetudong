<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl" title=canxe}
    <script type="text/javascript">
        function onload() {
            var timeInMs = Date.now();
        }

        function getLoad() {
            var rows = document.getElementById('rows').value;
            var respondData = $.ajax({
                type: 'GET',
                url: "vehicle.php?list=list&rows=" + rows,
                dataType: 'html',  // assuming your service returns HTML
                success: function (data) {
                    $("#table tbody").html(data);
                }
            });
        }

        //Event click button
        jQuery(document).ready(function ($) {
            $('.btStart').click(function (e) {


                var timeRefresh = document.getElementById('timeRefresh').value; //seconds

                setInterval('getLoad()', timeRefresh * 1000);// refresh div after timeRefresh seconds
            });
        });

    </script>
</head>
<body>
{include file="menu.tpl" title=foo}

<div class="container-fluid">
    <div class="live" ng-controller="liveController">

        <div class="row">
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Trạm</span>
                <select class="form-control" name="station">
                    {foreach from=$listStation item="st"}
                        <option value="{$st.Station_ID}">{$st.Name}</option>
                    {/foreach}

                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Tủ</span>
                <select class="form-control" name="cabinet">
                    {foreach from=$listCabinet item="ca"}
                        <option value="{$ca.Cabinet_ID}">{$ca.Name}</option>
                    {/foreach}

                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Làn </span>
                <select class="form-control">
                    {foreach from=$listLane item="la"}
                    <option value="{$la.Lane_ID}">{$la.Name}</option>
                    {/foreach}
                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Số dòng</span>
                <select class="form-control" id="rows" name="rows">
                    <option value="3">3</option>
                    <option value="5">5</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="150">150</option>
                    <option value="200">200</option>
                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Thời gian làm mới</span>
                <select class="form-control" id="timeRefresh" name="timeRefresh">
                    <option value="1">1s</option>
                    <option value="2">2s</option>
                    <option value="3">3s</option>
                    <option value="5">5s</option>
                    <option value="10">10s</option>
                    <option value="15">15s</option>
                </select>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 text-center">
                <p>Báo động quá tải</p>

                <div class="btn-group btn-toggle">
                    <button id="bt-off" class="btn btn-xs btn-info" onclick="playAudioInfo()">Bật</button>
                    <button id="bt-on" class="btn btn-xs btn-default bt-on" onclick="playAudioWarning()"> Tắt</button>
                    <audio id="audioInfo">
                        <source src="templates/audio/overload.wav"
                                type='audio/wav'>
                        Your user agent does not support the HTML5 Audio element
                    </audio>
                    <audio id="audioWarning">
                        <source src="templates/audio/warning.wav" type="audio/wav">
                        Your user agent does not support the HTML5 Audio element
                    </audio>
                </div>

            </div>
        </div>

        <br>
        <br/>

        <div class="">
      <span ng-switch on="isStart">
        <span ng-switch-when="false">
            <input id="btStart" type="button" class="btStart btn btn-primary" value="Bắt đầu">
        </span>

      </span>
      <span on="isLoading">
        <span class="update-text"> </span>
        <span id="ajaxload"><img src="templates/img/ajax-loader.giff" alt="Đang tải"/></span>
      </span>
        </div>
        <br>


        <div class="clearfix"></div>
        <div class="scroll">
            <table class="table table-bordered table-nowrap" id="table">
                <thead>
                <tr class="active">
                    <th>Chi tiết</th>
                    <th>STT</th>
                    <th>Hình ảnh</th>
                    <th>Bảng số xe</th>
                    <th>Ngày giờ</th>
                    <th>Chiều dài xe</th>
                    <th>Tổng tải trọng</th>
                    <th>Tổng tải trọng cho phép</th>
                    <th>(%)Quá tải tổng tải trọng</th>
                    <th>Số trục</th>
                    <th>Tải trọng trục lớn nhất</th>
                    <th>Tải trọng trục cho phép</th>
                    <th>(%)Quá tải trục</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
</div>
{include file="footer.tpl" title=footer}
<script type="text/javascript">

    var audioInfo = document.getElementById("audioInfo");
    var audioWarning = document.getElementById("audioWarning");
    function playAudioInfo() {
        audioInfo.play();
    }

    function stopAudioInfo() {
        audioInfo.pause();
    }

    function playAudioWarning() {
        audioWarning.play();
    }
    function stopAudioWarning() {
        audioWarning.pause();
    }

</script>
</body>
</html>