<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl"}
    <script type="text/javascript" src="templates/js/jquery.dataTables.min.js"></script>
    {*<script type="text/javascript" src="templates/js/datatables.js"></script>*}
    <script type="text/javascript">
        function getLoad() {
            var rows = document.getElementById('rows').value;
            var respondData = $.ajax({
                type: 'GET',
                url:"loaddata.php?respon=list&rows="+rows,
                dataType: 'html',  // assuming your service returns HTML
                success: function(data) {
                    $("#table tbody").html(data);
                }
            });

        }

        jQuery(document).ready(function ($) {
            $('#btStart').click(function (e) {
                var timeRefresh = document.getElementById('timeRefresh').value; //seconds

                setInterval('getLoad()', timeRefresh*1000);// refresh div after timeRefresh seconds
            });
        });

    </script>
    <!--Begin script phan trang-->

    <script src="templates/js/jquery-DT-pagination.js" type="text/javascript" language="javascript"></script>

    <script type="text/javascript">
        /* Table initialisation */
        $(document).ready(function () {
            $('#example').dataTable({
                "bSort": false,      // Disable sorting
                "iDisplayLength": 15,   //records per page
                "sDom": "t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap"
            });
        });
    </script>
    <!--End  script phan trang-->
</head>
<body>
{include file="menu.tpl" title=loaddata}

<div class="container-fluid">
    <div class="live" ng-controller="liveController">
        <h3>Lọc và xem dữ liệu trực tiếp</h3>

        <div class="row">
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Trạm</span>
                <select class="form-control" ng-model="record" ng-change="changeRecord(record)">
                    <option value="10">Trạm 1</option>
                    <option value="20">Trạm 2</option>

                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Tủ</span>
                <select class="form-control" ng-model="record" ng-change="changeRecord(record)">
                    <option value="10">Tủ 1</option>
                    <option value="20">Tủ 2</option>

                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Làn </span>
                <select class="form-control" ng-model="record" ng-change="changeRecord(record)">
                    <option value="10">Làn 1</option>
                    <option value="20">Làn 2</option>
                </select>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Số dòng</span>
                <select class="form-control" id="rows" name="rows">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>

                </select>
            </div>
            <div class="selector col-xs-12 col-sm-2 col-md-2">
                <span>Thời gian làm mới</span>
                <select class="form-control" ng-model="timeRefresh" id="timeRefresh" name="timeRefresh">
                    <option value="1">1s</option>
                    <option value="2">2s</option>
                    <option value="3">3s</option>
                    <option value="5">5s</option>
                    <option value="10">10s</option>
                    <option value="15">15s</option>
                </select>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 text-center">
                <p>Báo động quá tải</p>

                <div class="btn-group btn-toggle">
                    <button id="bt-off" class="btn btn-xs btn-info" onclick="stopAudioInfo()">Tắt</button>
                    <button id="bt-on" class="btn btn-xs btn-default" onclick="playAudioInfo()">Bật</button>
                    <audio id="audioInfo">
                        <source src="templates/audio/overload.wav"
                                type='audio/wav'>
                        Your user agent does not support the HTML5 Audio element
                    </audio>
                    <audio id="audioWarning">
                        <source src="templates/audio/warning.wav" type="audio/wav">
                        Your user agent does not support the HTML5 Audio element
                    </audio>
                </div>
                <div class="btn-group btn-toggle">
                    <button id="bt-off" class="btn btn-xs btn-default" onclick="stopAudioWarning()">Tắt</button>
                    <button id="bt-on" class="btn btn-xs btn-info" onclick="playAudioWarning()"> Bật</button>
                </div>
            </div>
        </div>
        <br/>

        <div class="">
      <span ng-switch on="isStart">
        <span ng-switch-when="false">
            <button id="btStart" type="button" class="btn btn-primary">Bắt đầu</button>
        </span>
        <span ng-switch-when="true">
            <button id="btStop" type="button" class="btn btn-danger" ng-click="stopLoad()">Dừng</button>
        </span>
      </span>
      <span ng-switch on="isLoading">
        <span ng-switch-when="false" class="update-text">Cập nhật vào: </span>
        <span ng-switch-when="true"><img src="templates/img/ajax-loader.gif" alt="Đang tải"/></span>
      </span>
        </div>

        <br>

        <div class="clearfix"></div>
        <div class="scroll">
            <table class="table table-striped table-bordered" id="table">
                <thead>
                <tr>
                    <th>Chi tiết</th>
                    <th>STT</th>
                    <th>Hình ảnh</th>
                    <th>Bảng số xe</th>
                    <th>Ngày giờ</th>
                    <th>Chiều dài xe</th>
                    <th>Tổng tải trọng</th>
                    <th>Tổng tải trọng cho phép</th>
                    <th>(%)Quá tải tổng tải trọng</th>
                    <th>Số trục</th>
                    <th>Tải trọng trục lớn nhất</th>
                    <th>Tải trọng trục cho phép</th>
                    <th>(%)Quá tải trục</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
</div>

{include file="footer.tpl" title=footer}

</body>
</html>