<script type="text/javascript" src="templates/js/jquery.min.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmCreateUser').validate(
                {
                    rules: {

                        name: {
                            minlength: 2,
                            required: true
                        },
                        password: {
                            required: true
                        },
                        repassword: {
                            required: true,
                            equalTo: "#password"
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {

                        name: "(*)",
                        password:"(*)",
                        email:"(*)",
                        repassword:"(*)"
                }

                });
    });

</script>
<script type="text/javascript">
    function checkEmail(email) {
        var xmlhttp;

        if(email.length==0)
        {
            document.getElementById("alertEmail").innerHTML="";
            return;
        }
        try {
            xmlhttp=new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        xmlhttp.onreadystatechange=function() {
            if(xmlhttp.readyState==4 && xmlhttp.status==200) {

                document.getElementById("alertEmail").innerHTML=xmlhttp.responseText;
            }
        }
        var url='users.php?checkE=checkEmail&email='+email+'';
        xmlhttp.open("GET",url,true);
        xmlhttp.send();
    }
    function checkUser(username) {
        var xmlhttp;

        if(username.length==0)
        {
            document.getElementById("alertUser").innerHTML="";
            return;
        }
        try {
            xmlhttp=new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        xmlhttp.onreadystatechange=function() {
            if(xmlhttp.readyState==4 && xmlhttp.status==200) {

                document.getElementById("alertUser").innerHTML=xmlhttp.responseText;
            }
        }
        var url='users.php?checkU=checkUser&u='+username+'';
        xmlhttp.open("GET",url,true);
        xmlhttp.send();
    }
</script>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Thêm tài khoản</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="users.php?action=addUser" id="frmCreateUser"
                  name="frmCreateUser">
                <div class="control-group">

                </div>
                <div class="control-group">
                    <label class="control-label">Tên *</label>
                    <div class="controls">
                        <input class="input-xlarge required" id="name" name="name" placeholder="Nhập tên" maxlength="199">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tài khoản</label>
                    <div class="controls">
                        <input class="input-xlarge required" id="username" name="username" placeholder="Nhập tên" maxlength="199" onkeyup="checkUser(this.value)">
                        <label id="alertUser" style="color: red;font-weight: normal;font-size: 13px"></label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Email  *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="email" name="email" class="required email form-control"
                                maxlength="199" placeholder="Nhập địa chỉ email" onkeyup="checkEmail(this.value);">
                        <label id="alertEmail" style="color: red;font-weight: normal;font-size: 13px"></label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Mật khẩu *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="password" name="password" type="password"
                               placeholder="Mật khẩu" maxlength="200"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Xác nhận mật khẩu*</label>
                    <div class="controls">
                        <input class="input-xlarge" name="repassword" type="password"
                               placeholder="Xác nhận mật khẩu"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Quyền </label>
                    <div class="controls">
                        <select name="role">
                            <option value="mod">Mod</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Thêm </button>
                </div>
            </form>
        </div>
    </div>
</div>
