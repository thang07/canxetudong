<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    {include file="head.tpl" title=login}
</head>
<body class="body-login">
<div class="round-login">
    <div class="login-with400">
        <div class="title-login"><i class="icon-lock"></i> Đăng nhập</div>
        <form class="form-horizontal" role="form" method="post" action="login.php">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên đăng nhập</label>
                <input type="text" class="form-control" name="username">
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Mật khẩu</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="btn-login">
                <button type="submit" class="btn btn-primary" value="Login" name="btnlogin">Đăng nhập</button>

            </div>
        </form>
    </div>
</div>

</body>
</html>