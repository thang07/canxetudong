<script type="text/javascript" src="templates/js/jquery.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmUpdateUser').validate(
                {
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },


                        email: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        name: "(*)",
                        email: "(*)"

                    }

                });
    });
</script>
<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Cập nhật User </div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="users.php?actionUpdate=updateUser" id="frmUpdateUser"
                  name="frmCreateUser">
                {foreach from=$getUserId item="i"}
                    <input type="hidden" value="{$i.id}" name="id"/>
                    <div class="control-group">
                        <label class="control-label" for="name"> Tên *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="name" value="{$i.name}" name="name"
                                   placeholder="Nhập tên" maxlength="199">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email">Email *</label>

                        <div class="controls">
                            <input class="input-xlarge" id="email" value="{$i.email}" name="email"
                                   class="required email form-control"
                                   type="email" placeholder="Nhập email" maxlength="200">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">User Role </label>

                        <div class="controls">
                            <select name="role">
                                {if $i.role eq 'admin'}
                                    <option value="{$i.role}" selected="selected">Admin</option>

                                    <option value="mod">Mod</option>
                                {elseif $i.role eq 'mod'}
                                    <option value="{$i.role}" selected="selected">Mod</option>
                                    <option value="admin">Admin</option>

                                {/if}
                            </select>
                        </div>
                    </div>

                {/foreach}
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnUpdate">Cập nhật</button>
                </div>
            </form>
        </div>
    </div>
</div>
