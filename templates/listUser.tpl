<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl" title=canxe}
    <script src="templates/js/jquery.colorbox.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            PopupUser();
            PopupUpdateUser();
        });
        // Popup user form
        function PopupUser() {
            $(".createUser").colorbox(
                    {
                        width: "500px"
                    });
        }
        //Popup update form
        function PopupUpdateUser(){

            $(".editUser").colorbox(
                    {
                        width: "500px"
                    });
        }
        //ajax request del user
        function delUser(id) {
            var answer = confirm("Bạn muốn xóa ?")
            if (answer) {
                $.ajax
                ({
                    url: "users.php?delUser=delUser&id=" + id,
                    success: function () {
                        location.reload();
                    }
                });
            }
        }
    </script>
</head>
<body>
{include file="menu.tpl" title=user}

<div class="container">
    <div class="col-md-12">
        <div class="widget wred">
            <div class="widget-header">
                <div class="pull-left"><i class="icon-user"></i> Danh Sách User</div>
                <div class="widget-icons pull-right">
                    <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a>
                    <a class="wclose" href="#"><i class="icon-remove"></i></a>
                </div>
                <div class="clearfix"> &nbsp&nbsp<a href="users.php?frmAdd=addNew" class="createUser" title="Thêm user ">
                        <i class="icon-plus-sign"></i>&nbsp&nbspThêm User</a></div>
            </div>
            <div class="widget-content">
                <table class="table table-bordered" id="example">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tên user </th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Quyền</th>
                        <th class="title-action">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var="stt" value="1"}
                    {foreach from=$listUser item="i"}
                        <tr class="record" id="record">
                            <td>{$stt++}</td>
                            <td>{$i.name}</td>
                            <td>{$i.username}</td>
                            <td>{$i.email}</td>
                            <td>{$i.role}</td>

                            <td class="td-action">
                                <a href="users.php?show=frmUpdate&&id={$i.id}" title="Cập nhật" class="editUser"><i
                                            class="icon-pencil"></i></a>
                                <a href="#" title="Xóa" id="{$i.id}" onclick="delUser('{$i.id}')" class="delbutton">
                                    <i class="icon-trash"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl" title=footer}
</body>
</html>

