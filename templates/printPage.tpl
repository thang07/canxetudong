<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl"}
</head>
<body>

<div class="container">

    <div id="div-print" class="top-buffer">
        <div>
            <div class="pull-left text-center">
                <span>Sở GTVT TP.Hồ Chí Minh</span><br />
                <span><b>Trạm KTTTTX Lưu động số: </b></span>
            </div>
            <div class="pull-right text-center">
                <span>Lý trình: Chưa nhập</span><br />
                <span>Lần cân: 1</span>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="text-center">
            <h3>PHIẾU CÂN XE</h3>
            <span> <b>Số: </b></span>
        </div>
        <div>
            <div class="col-xs-8 col-sm-8 col-md-8">
                <table class="table-print">
                    <tr>
                        <td>Hình thức:</td>
                        <td>Kiểm tra theo tải trọng cho phép của cầu, đường.</td>
                    </tr>
                    <tr>
                        <td>Biển số xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Loại xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Họ tên lái xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Giấy phép lái xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Chủ xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tốc độ xe</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Số trục xe:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Thời gian xe vào:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Thời gian xe ra:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Ngày cân:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>GP.QK/QT số:</td>
                        <td>Chưa nhập</td>
                    </tr>
                    <tr>
                        <td>Chế độ cân</td>
                        <td>Cân động</td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <img src="" alt="Xe Tai" class="img-thumbnail" width="100%"/>
                <div class="text-center"><b>Hình: </b></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <br />
        <div>
            <table class="table table-bordered">
                <thead>
                <tr class="active text-center">
                    <th class="text-center">STT</th>
                    <th class="text-center">Bộ trục xe</th>
                    <th class="text-center">Tải trọng bộ trục [Tấn]</th>
                    <th class="text-center">Sai số cho phép [Tấn]</th>
                    <th class="text-center">Tải trọng trừ sai số [Tấn]</th>
                    <th class="text-center">Tải trọng cho phép [Tấn]</th>
                    <th class="text-center">Tải trọng quá tải [Tấn]</th>
                    <th class="text-center">Kết luận quá tải [%]</th>
                </tr>
                </thead>
                <tr ng-repeat="axles in printItem.listAxles">
                    <td></td>
                    <td>Đơn</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="2">Tổng cả xe</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div>
            <p>Kết luận: </p>
            <p>Ghi chú:</p>
        </div>
        <br />
        <div>
            <div class="pull-left text-center">
                <span><b>Người lái xe (Chủ xe hoặc người đại diện)</span></b><br />
                <span><i>(Ký ghi rõ họ tên)</i></span>
            </div>
            <div class="pull-right text-center">
                <span><b>Người lập phiếu</span></b><br />
                <span><i>(Ký ghi rõ họ tên)</i></span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</body>
</html>