<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl"}
    <script src="templates/js/jquery.colorbox.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            PopupStation();
            PopupUpdateStation();
        });
        // Popup user form
        function PopupStation() {
            $(".createStation").colorbox(
                    {
                        width: "500px"
                    });
        }

        function PopupUpdateStation(){
            $(".editStation").colorbox(
                    {
                        width: "500px"
                    });
        }
        //ajax request del user
        function delStation(id) {
            var answer = confirm("Bạn muốn xóa ?")
            if (answer) {
                $.ajax
                ({
                    url: "station.php?delStation=del&id=" + id,
                    success: function () {
                        location.reload();
                    }
                });
            }
        }
    </script>
</head>
<body>
{include file="menu.tpl" title=user}

<div class="container">
    <div class="col-md-12">
        <div class="widget wred">
            <div class="widget-header">
                <div class="pull-left"><i class="icon-user"></i> Danh Sách Trạm</div>
                <div class="widget-icons pull-right">
                    <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a>
                    <a class="wclose" href="#"><i class="icon-remove"></i></a>
                </div>
                <div class="clearfix"> &nbsp&nbsp<a href="station.php?frmAdd=addStation" class="createStation"
                                                    title="Thêm user ">
                        <i class="icon-plus-sign"></i>&nbsp&nbspThêm Trạm</a></div>
            </div>
            <div class="widget-content">
                <table class="table table-bordered" id="example">
                    <thead>
                    <tr>
                        <th>StationID</th>
                        <th>LocationID</th>
                        <th>Tên</th>
                        <th>Weight</th>
                        <th>Mô tả</th>
                        <th class="title-action">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>

                    {foreach from=$listStation item="i"}
                        <tr class="record" id="record">
                            <td>{$i.Station_ID}</td>
                            <td>{$i.Location_ID}</td>
                            <td>{$i.Name}</td>
                            <td>{$i.Weight}</td>
                            <td>{$i.Description}</td>

                            <td class="td-action">
                                <a href="station.php?show=frmUpdate&&id={$i.Station_ID}" title="Cập nhật"
                                   class="editStation"><i
                                            class="icon-pencil"></i></a>
                                <a href="#" title="Xóa" id="{$i.Station_ID}" onclick="delStation('{$i.Station_ID}')"
                                   class="delbutton">
                                    <i class="icon-trash"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{include file="footer.tpl" title=footer}
</body>
</html>

