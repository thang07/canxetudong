<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl"}
    <script src="templates/js/jquery.colorbox.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            PopupCabinet();
            PopupUpdateCabinet();
        });
        // Popup user form
        function PopupCabinet() {
            $(".createStation").colorbox(
                    {
                        width: "500px"
                    });
        }

        function PopupUpdateCabinet(){
            $(".editStation").colorbox(
                    {
                        width: "500px"
                    });
        }

    </script>
</head>
<body>
{include file="menu.tpl"}

<div class="container">
    <div class="col-md-12">
        <div class="widget wred">
            <div class="widget-header">
                <div class="pull-left"><i class="icon-user"></i> Danh Sách Tủ</div>
                <div class="widget-icons pull-right">
                    <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a>
                    <a class="wclose" href="#"><i class="icon-remove"></i></a>
                </div>
                <div class="clearfix"> &nbsp&nbsp<a href="cabinet.php?frmAdd=addCabinet" class="createStation"
                                                    title="Thêm user ">
                        <i class="icon-plus-sign"></i>&nbsp&nbspThêm Tủ</a></div>
            </div>
            <div class="widget-content">
                <table class="table table-bordered" id="example">
                    <thead>
                    <tr>
                        <th>Cabinet_ID</th>
                        <th>Station_ID</th>
                        <th>Tên</th>
                        <th>Dataloger</th>
                        <th>Mô tả</th>
                        <th class="title-action">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>

                    {foreach from=$listCabinet item="i"}
                        <tr class="record" id="record">
                            <td>{$i.Cabinet_ID}</td>
                            <td>{$i.Station_ID}</td>
                            <td>{$i.Name}</td>
                            <td>{$i.Dataloger}</td>
                            <td>{$i.Description}</td>

                            <td class="td-action">
                                <a href="cabinet.php?show=frmUpdate&&id={$i.Cabinet_ID}" title="Cập nhật"
                                   class="editStation"><i
                                            class="icon-pencil"></i></a>
                                <a href="cabinet.php?del=del&&id={$i.Cabinet_ID}" title="Xóa" id="{$i.Cabinet_ID}" class="delbutton">
                                    <i class="icon-trash"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{include file="footer.tpl" title=footer}
</body>
</html>

