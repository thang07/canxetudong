<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl" title=canxe}
</head>
<body>
{include file="menu.tpl"}

<div class="container-fluid">
    {foreach from=$getVehicle item='i'}
        <div class="detail">
            <div>
                <div class="col-md-5">
                    <h4 class="col-xs-12 col-sm-12 col-md-12 text-left"><b>Số xe: </b></h4>

                    <div class="data-detail col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Xe tải
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <span class="glyphicon glyphicon-dashboard"></span>
                            Trọng lượng: {$i.vGross_Weight} kg
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <span class="glyphicon glyphicon-road"></span> {$i.vVelocity} km/h
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <span class="glyphicon glyphicon-time"></span> {$i.vTime}
                        </div>

                    </div>

                    <div class="row"></div>
                    <img src="templates/upload/{$i.pVehicle_Image}" alt="Xe Tai" class="img-thumbnail" width="100%"
                         height="auto"/>

                </div>

                <div class="col-md-7">
                    <h4>Kết quả</h4>
                    <table class="table result">
                        <tr class="result success-color">
                            <td>Vi phạm:</td>
                            <td class="pull-right">
                                <a href="printpage.php?get=print" class="btn btn-primary">In</a>
                            </td>
                        </tr>
                    </table>
                    <h4>Chi tiết xe</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th>Stt</th>
                            <td>{$i.vVerhicle_ID}</td>
                            <th>Chiều dài xe</th>
                            <td>{$i.vLenght} m</td>
                        </tr>
                        <tr>
                            <th>Ngày</th>
                            <td>{$i.vTime|date_format:"%d-%m-%Y"}</td>
                            <th>Khoảng cách</th>
                            <td> m</td>
                        </tr>
                        <tr>
                            <th>Giờ</th>
                            <td>{$i.vTime|date_format:"%H:%M:%S"}</td>
                            <th>Tải trọng trái</th>
                            <td>{$i.vLeft_Weight} kg</td>
                        </tr>
                        <tr>
                            <th>Làn</th>
                            <td></td>
                            <th>Tải trọng phải</th>
                            <td>{$i.vRight_Weight} kg</td>
                        </tr>
                        <tr>
                            <th>Tốc độ</th>
                            <td>{$i.vVelocity} km/h</td>
                            <th>Tổng tải trọng</th>
                            <td>{$i.vGross_Weight} kg</td>
                        </tr>
                        <tr>
                            <th>Hướng</th>
                            <td></td>
                            <th>Cân đối trái/phải</th>
                            <td>{$i.vLeft_Weight} / {$i.vRight_Weight}</td>
                        </tr>
                    </table>
                    <h4>Chi tiết trục xe</h4>

                        <table class="table table-bordered table-nowrap">
                            <tr>
                                <th>Stt</th>
                                <th>Trọng lượng trục</th>
                                <th>Cân đối trục trái/phải</th>
                                <th>Khoảng cách</th>
                                <th>(%)Quá tải</th>
                            </tr>
                            {foreach from=$getAxle item='ax'}
                            <tr class="">
                                <td>{$i.vVerhicle_ID}</td>
                                <td>{$ax.aWeight}</td>
                                <td>{$ax.aLeftWheelWeight} / {$ax.aRigthWheelWeight}</td>
                                <td>{$ax.aDistance} m</td>
                                <td>%</td>
                            </tr>
                            {/foreach}
                            <tr>
                                <th>Tổng</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </table>
                </div>
            </div>
        </div>
    {/foreach}
</div>
</body>
</html>