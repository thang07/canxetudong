<!DOCTYPE HTML>
<html>
<head>
    {include file="head.tpl" title=foo}
</head>
<body>
{include file="menu.tpl" title=foo}
<div class="history container-fluid" ng-controller="historyController" ng-init="">
    <div>
        <div class="col-xs-12 col-sm-11 col-md-11" style="padding-left: 0px;">
            <h3>Lọc và xem danh sách xe đã chạy qua cân</h3>
        </div>
        <div class="col-xs-12 col-sm-1 col-md-1 pull-right">
            <span id="loading">
                <img src="/templates/img/ajax-loader-big.gif" alt="Đang tải"/>
            </span>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="selector col-xs-12 col-sm-4 col-md-4">
            <div class="selector col-xs-12 col-sm-4 col-md-4">
                <span>Số dòng</span>
                <select class="form-control" ng-model="recordHistory" ng-change="initHistory()">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
            <div class="selector col-xs-12 col-sm-4 col-md-4">
                <span>Số Trục</span>
                <select class="form-control" ng-model="numAxlesHistory" ng-change="initHistory()">
                    <option value="0">Tất cả</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                </select>
            </div>
            <div class="selector col-xs-12 col-sm-4 col-md-4">
                <span>Trạng thái</span>
                <select class="form-control" ng-model="statusHistory" ng-change="initHistory()">
                    <option value="0">Tất cả</option>
                    <option value="1">Cảnh báo</option>
                    <option value="2">Vi phạm</option>
                </select>
            </div>
        </div>
        <div class="selector col-xs-12 col-sm-3 col-md-4">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <span>Từ ngày</span>

                <div class="" id="dateTimeFrom">
                    <input class="form-control datetime" id="dtFrom" type="text" ng-model="dtFrom" data-format="dd/MM/yyyy"/>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <span>Đến ngày</span>

                <div class="" id="dateTimeTo">
                    <input class="form-control datetime" id="dtTo" type="text" ng-model="dtTo" data-format="dd/MM/yyyy"/>
                    <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>
        </div>
        <div class="selector col-xs-12 col-sm-2 col-md-1">
            <button type="button" class="btn btn-info btn-history" ng-click="initHistory()">Xem</button>
        </div>
        <div class="selector col-xs-12 col-sm-3 col-md-3">
            <span>Tìm kiếm</span>

            <div class="input-group">
                <input id="search" type="text" ng-model="searchVal" class="form-control" placeholder="Bảng số xe..."/>
                <span class="input-group-btn">
                    <button class="btn btn-info" type="button" ng-click="search()">Tìm</button>
                </span>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br/>

    <div class="scroll">
        <table class="table table-bordered table-nowrap">
            <thead>
            <tr class="active">
                <th>Chi tiết</th>
                <th>STT</th>
                <th>Hình ảnh</th>
                <th>Bảng số xe</th>
                <th>Ngày giờ</th>
                <th>Chiều dài xe</th>
                <th>Tổng tải trọng</th>
                <th>Tổng tải trọng cho phép</th>
                <th>(%)Quá tải tổng tải trọng</th>
                <th>Số trục</th>
                <th>Tải trọng trục lớn nhất</th>
                <th>Tải trọng trục cho phép</th>
                <th>(%)Quá tải trục</th>
            </tr>
            </thead>

            <tr class="" ng-repeat="data in historyData">
                <td><a href="">Xem</a></td>
                <td></td>
                <td><img src="" alt="Hình "/></td>
                <td></td>
                <td></td>
                <td> m</td>
                <td></td>
                <td></td>
                <td>%</td>
                <td></td>
                <td></td>
                <td></td>
                <td>%</td>

            </tr>
        </table>
    </div>

</div>
{include file="footer.tpl" title=footer}
<script type="text/javascript">
    $(function () {
        $('#dateTimeFrom').datetimepicker({
            locale: 'vi'
        });
        $('#dateTimeTo').datetimepicker({
            locale: 'vi'
        });
    });
</script>

</body>
</html>