<script type="text/javascript" src="templates/js/jquery.min.js"></script>
<script type="text/javascript" src="templates/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#frmCreateUser').validate(
                {
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },
                        weight: {
                            required: true
                        }
                    },
                    messages: {
                        name: "(*)",
                        weight:"(*)"
                    }
                });    });
</script>

<div class="col-md-12">
    <div class="widget-blue">
        <div class="widget-header-blue"><i class="icon-user"></i>Thêm tủ</div>
        <div class="widget-body">
            <form class="form-horizontal no-margin" method="post" action="cabinet.php?action=addCabinet" id="frmCreate"
                  name="frmCreateUser">
                <div class="control-group">
                </div>
                <div class="control-group">
                    <label class="control-label">Trạm</label>
                    <div class="controls">
                        <select name="station">
                            {foreach from= $listStation item='i'}
                                <option value="{$i.Station_ID}">{$i.Name}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tên  *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="name" name="name" class="required form-control"
                               maxlength="199" placeholder="Nhập tên" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Dataloger  *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="dataloger" name="dataloger" class="required form-control"
                               maxlength="199" placeholder="Nhập dataloger" >
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Mô tả  *</label>
                    <div class="controls">
                        <input class="input-xlarge" id="description" name="description" class="required form-control"
                               maxlength="199" placeholder="Nhập Description" >
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-info pull-right" name="btnCreate">Thêm </button>
                </div>
            </form>
        </div>
    </div>
</div>
