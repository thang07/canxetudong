<div class="wapper">
    <div class="header">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#wim-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Hệ thống quản lý cân xe tự động</a>
                </div>
                <div class="collapse navbar-collapse" id="wim-menu">
                    <ul class="nav navbar-nav">
                        <!--Phân quyền user -->
                        {if $smarty.session.Role eq 'mod'}
                            <li><a href="cabinet.php?list=listCabinet">Tủ</a></li>

                        {elseif $smarty.session.Role eq 'admin'}

                            <li><a href="history.php?list=listHistory">Lịch sử</a></li>
                            <li><a href="users.php?list=listUser">Người dùng</a></li>
                            <li><a href="station.php?list=listStation">Trạm</a></li>
                            <li><a href="cabinet.php?list=listCabinet">Tủ</a></li>

                        {/if}
                    </ul>
                    <ul ng-switch on="isLogin" class="nav navbar-nav navbar-right">
                        <li id="logout" ng-switch-when="true">
                            <a href="logout.php?logout=out" ng-click="" id="logout">Xin chào {$smarty.session.Login}
                                - Đăng xuất</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>