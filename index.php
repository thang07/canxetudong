<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/26/2015
 * Time: 9:20 PM
 */


require "include/smarty.php";
require "include/users_func.php";
require "include/station_func.php";
require "include/cabinet_func.php";
require "include/lane_func.php";

if (!isset($_SESSION['Login'])) {
    $smarty->assign("title", "Login page");
    header("location:login.php");//Chuyen huong trang login

} else {
    $listStation=getListStation(20);
    $listCabinet=getListCabinet(20);
    $listLane=getListLane(10);

    $smarty->assign("listStation",$listStation);
    $smarty->assign("listCabinet",$listCabinet);
    $smarty->assign("listLane",$listLane);
    $smarty->assign("username",$_SESSION['Login']);
    $smarty->assign("title","Page Management");
    $smarty->display('index.tpl');
}