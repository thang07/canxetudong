<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/27/2015
 * Time: 1:26 PM
 */
require "include/smarty.php";
require "include/users_func.php";


if(!isset($_SESSION['Login'])){
    header("location:login.php");
}

//Display list user
else if(isset($_GET['list'])=="listUser"){
    $list=getListUser();
    $smarty->assign("listUser",$list);
    $smarty->assign("title","Quan ly User");
    $smarty->display('listUser.tpl');
}

//Display form user
else if(isset($_REQUEST['frmAdd'])=='addNew'){
    $smarty->assign("title","Thêm user");
    $smarty->display('addUser.tpl');
}

//Check email
else if(isset($_REQUEST['checkE'])=='checkEmail'){
    $email = $_GET['email'];
    if (checkEmail($email)) {
        echo "Email này đã tồn tại, try other";
    }
}

//Check user name
else if(isset($_REQUEST['checkU'])=='checkUser'){
        $username=$_GET['u'];
    if(checkUser($username)){
        echo "Username này đã tồn tại, try other";
    }
}

//Insert new user
else if(isset($_GET['action']) == "addUser"){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $username = $_POST['username'];
    $role = $_POST['role'];
    $page='10';
    $values = array($username, sha1($password),$email,$name,$role);
    if (checkEmail($email) == 0 && checkUser($username)==0) {
        $rs = insertUser($values);
        if ($rs == true) {
            $listUser = getListUser();
            $smarty->assign("listUser", $listUser);
            $smarty->assign('title', 'Danh Sách User');
            $smarty->clearCache('listUser.tpl');
            header('location:users.php?list=listUser');
        }
    } else {
        echo "<script language='javascript'>
            alert('Email hoặc Username đã tồn tại ');
            javascript:window.history.back(-1);
        </script>";
    }

}

//Show form update user
else if(isset($_GET['show'])=='frmUpdate'){
    $getUserId = getUserId($_REQUEST['id']);
    $smarty->assign("getUserId", $getUserId);
    $smarty->assign('title', 'Cập nhật User');
    $smarty->display('updateUser.tpl');
}

//Action update user
else if(isset($_REQUEST['actionUpdate'])=="updateUser"){
    $id_user = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $role = $_POST['role'];
    $values = array($email,$name, $role, $id_user);
    $result = updateUser($values);
    $smarty->assign("title", "Danh Sách User");
    header('location:users.php?list=listUser');//điều hướng về trag danh sách user
}

//delete user by id
else if(isset($_REQUEST['delUser'])=='delUser'){
    if ($_GET['id']) {
        $_id_user = $_GET['id'];
        deleteUser($id_user);
    }
}
else{
    header("location:index.php");
}