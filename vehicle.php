<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/30/2015
 * Time: 11:28 AM
 */
require "include/smarty.php";
require "include/vehicle_func.php";

if (isset($_GET['list']) == 'list') {
    $num_row = $_GET['rows'];

    $vehicle = array();
    $vehicle = getListVehicle($num_row);
    $stt = 0;
    foreach ($vehicle as $i) {
        $stt = $stt + 1;
        $format = new DateTime($i['vTime']);
        $date = date_format($format, 'd/m/Y H:m:s');
        $vid = $i['vVerhicle_ID'];
        $img = $i['pVehicle_Image'];

        if ($stt == 1) {
            echo "<tr>";
            echo "<td colspan='6' class='text-right'>";
            echo "<img src='templates/upload/$img' class='imgVehicle' height='200px' width='auto'>";
            echo "</td>";
            echo "<td colspan='7'>";

            echo "<div class='colLeft'>";

            echo "<div class='rows'>";
            echo "<p class='left'> Ngày: " . $date . "</p>";
            echo "<p class='right'>";
            if (($i['vAxles_Count']) == '1') {

                echo "<img src = 'templates/img/truck1.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '2') {

                echo "<img src = 'templates/img/truck2.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '3') {

                echo "<img src = 'templates/img/truck3.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '4') {

                echo "<img src = 'templates/img/truck4.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '5') {

                echo "<img src = 'templates/img/truck5.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '6') {

                echo "<img src = 'templates/img/truck6.png' class='truck'/> ";
            } else {

                echo "<img src = 'templates/img/truck7.png' class='truck'/> ";
            }
            echo "</p>";
            echo "</div>";

            echo "<div class='rows'>";
            echo "<p class='left'> BSX: " . $i['pPlate_Name'] . "</p>";
            echo "<p class='right'> Trọng tải : " . $i['vGross_Weight'] . " kg </p>";
            echo "</div>";

            echo "<div class='rows'>";
            echo "<p class='left'>Tốc độ:" . " km/h </p>";
            echo "<p class='right'>TTT: " . $i['vGross_Weight'] . "</p>";
            echo "</div>";
            echo "<h4> Vi Phạm : " ."</h4>";
            echo "<div class='wrapper-warning'>";

            echo "<div class='warning rows bgreen text-center'>". "Không quá tải " ."</div>";

            echo "<div class='warning rows bgred text-center'>" ." > 10 %"."</div>";

            echo "<div class='warning rows bgyellow text-center'>" ."<10 %"."</div>";

            echo "</div>";
            echo "</div>";
            echo "</td>";
            echo "</tr>";

        } else {

            echo "<tr>";
            echo "<td>  <a href='detail.php?vId=$vid'> Xem </a> </td>";
            echo "<td>" . $i['vVerhicle_ID'] . "</td>";
            echo "<td>";
            if (($i['vAxles_Count']) == '1') {

                echo "<img src = 'templates/img/truck1.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '2') {

                echo "<img src = 'templates/img/truck2.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '3') {

                echo "<img src = 'templates/img/truck3.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '4') {

                echo "<img src = 'templates/img/truck4.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '5') {

                echo "<img src = 'templates/img/truck5.png' class='truck'/> ";
            } else if (($i['vAxles_Count']) == '6') {

                echo "<img src = 'templates/img/truck6.png' class='truck'/> ";
            } else {

                echo "<img src = 'templates/img/truck7.png' class='truck'/> ";
            }
            echo "</td>";
            echo "<td>" . $i['pPlate_Name'] . "</td>";
            echo "<td>" . $date . "</td>";
            echo "<td>" . $i['vLenght'] . " m </td>";
            echo "<td>" . $i['vGross_Weight'] . " kg </td>";
            echo "<td>" . ($i['vbWeight'] * 1000) . "kg </td>";
            echo "<td>" . " % </td>";
            echo "<td>" . $i['vAxles_Count'] . "</td>";
            echo "<td>" . $i['aWeight'] . "</td>";
            echo "<td>" . ($i['abWeight'] * 1000) . "</td>";
            echo "<td>" . " % </td>";
            echo "</tr>";
        }
    }


}