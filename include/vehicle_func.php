<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/30/2015
 * Time: 11:27 AM
 */

require_once 'functions.php';

function getListVehicle($rows){
    $db = connectdb();

    $str="SELECT  v.Verhicle_ID as 'vVerhicle_ID',v.Lane_ID,v.Lane_ID,v.Lane_ID, v.Time as 'vTime',v.Lenght as 'vLenght',
                  v.Gross_Weight as 'vGross_Weight',
                  v.Axles_Count as 'vAxles_Count',
                  p.Plate_Name as 'pPlate_Name',
                  a.Weight as 'aWeight',
                  vb.Weight as 'vbWeight',
                  ab.Weight as 'abWeight',
                  p.Vehicle_Image as 'pVehicle_Image'

          FROM verhicle v,plate p , axles a, verhiclebase vb, axlesbase ab
          WHERE v.Plate_ID=p.Plate_ID
          AND v.Verhicle_ID=a.Verhicle_ID
          AND v.Verhicle_Base_ID= vb.Verhicle_Base_ID
          AND a.Axles_Base_ID= ab.Axles_Base_ID
          AND v.Verhicle_ID= a.Verhicle_ID
          ORDER by v.Time DESC
          LIMIT " .$rows;

    $stm = $db->prepare($str);
    $result = $stm->execute();
    $result=$stm->fetchAll();
    return $result;

}