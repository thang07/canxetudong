<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/27/2015
 * Time: 9:17 AM
 */

require_once 'functions.php';


//function login user
function loginUser($value, $pass)
{
    $db = connectdb();//connect db
    $stmt = $db->prepare("SELECT * from users where username='$value' and password='$pass'");
    $stmt->execute();
    $result = $stmt->fetchAll();
    if ($result != false) {
        $_username = "";
        $_pass = "";
        $_role = "";
        $_user_id = "";

        foreach ($result as $row) {
            $_username = $row["username"];
            $_pass = $row["password"];
            $_email = $row["email"];
            $_role = $row["role"];
            $_user_id = $row['id'];
        }
        if (($_username == $value) && ($_pass == $pass)) {
            // if (($_email == $value || $_phone==$value) && ($pass == $_pass)){
            $_SESSION['User_id'] = $_user_id;
            $_SESSION['Login'] = $_username;
            $_SESSION['Role'] = $_role;
            $_SESSION['Email'] = $_email;
            $_SESSION['start'] = time();
            $_SESSION['expire'] = $_SESSION['start'] + (60 * 60);
            return true;

        } else {
            return false;
        }
    }
}

//function check role of user
function checkRole($id, $role)
{
    $db = connectdb();
    $stmt = $db->prepare("select role from users where id=? and role=?");
    $stmt->execute(array($id, $role));
    if ($result = $stmt->fetchAll()) {
        return true;
    } else {
        return false;
    }
}

//function check email of users
function checkEmail($_email)
{
    $db = connectdb();
    $stm = $db->prepare("select email from users where email=? limit 100 ");
    $stm->execute(array($_email));
    $result = $stm->fetchAll();
    if ($stm->rowCount() > 0) {
        $email = "";
        foreach ($result as $row) {
            $email = $row['email'];
        }
        if ($_email == $email) {
            return true;
        } else {
            return false;
        }
    }
}

//function check username
function checkUser($_username)
{
    $db = connectdb();
    $stm = $db->prepare("select username from users where username=?");
    $stm->execute(array($_username));
    $result = $stm->fetchAll();
    if ($stm->rowCount() > 0) {
        $username = "";
        foreach ($result as $row) {
            $username = $row['username'];
        }
        if ($_username == $username) {
            return true;
        } else {
            return false;
        }
    }
}

//function get list users
function getListUser()
{
    $query=fselect("users","id","10");
    $results=$query->fetchAll();
    return $results;
}

//function insert user
function insertUser($value)
{
    $str = "INSERT INTO users(username,password,email,name,role) values(?,?,?,?,?)";
    if (finsert($str, $value)) {
        return true;
    } else return false;
}
//Get user by id
function getUserId($id){
    $query="SELECT * FROM users WHERE id=?";
    $result=fselect_id($query,$id);
    return $result->fetchAll();

}
//function update user
function updateUser($value){
    $query="UPDATE users SET email=?, name=?, role=? WHERE id=?";
    if(fupdate($query,$value)){
        return true;
    }else{
        return false;
    }
}
//delete user by id
function deleteUser($id){
    if(fdelete("users","id",$id)) //table users
    {
        return true;
    }else
        return false;
}



