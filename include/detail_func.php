<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/28/2015
 * Time: 4:20 PM
 */

require_once('functions.php');

//Lay chi tiet xe
function getVehicleDetailById($id)
{
    $db = connectdb();
    $str = "SELECT v.Verhicle_ID as 'vVerhicle_ID',v.Time as 'vTime',
                    v.Lenght as 'vLenght', v.Left_Weight as 'vLeft_Weight',
                    v.Right_Weight as 'vRight_Weight', v.Gross_Weight as 'vGross_Weight',
                    v.Velocity as 'vVelocity',
                    p.Vehicle_Image as 'pVehicle_Image'
            FROM
            verhicle v,plate p
            WHERE
            v.Plate_ID=p.Plate_ID
            AND v.Verhicle_ID=?";
    $result=fselect_id($str,$id);
    return $result->fetchAll();

}

//chi tiet truc xe

function getAxlesById($id)
{
    $db = connectdb();
    $str="
        SELECT a.Weight as 'aWeight', a.LeftWheelWeight as 'aLeftWheelWeight', a.RigthWheelWeight as 'aRigthWheelWeight',
                a.Distance as 'aDistance'
         FROM  axles a,axlesbase ab, verhicle v
         WHERE a.Axles_Base_ID=ab.Axles_Base_ID
          AND v.Verhicle_ID=a.Verhicle_ID
          AND a.Verhicle_ID=?
    ";
    $result=fselect_id($str,$id);
    return $result->fetchAll();
}