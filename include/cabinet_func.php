<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/29/2015
 * Time: 4:20 PM
 */


require_once 'functions.php';

//get list cabinet
function getListCabinet($page)
{
    $query = fselect("cabinet", "Cabinet_ID", $page);
    $results = $query->fetchAll();
    return $results;
}

//insert cabinet

function insertCabinet($values)
{
    $str = "INSERT INTO CABINET (Station_ID,Name,Dataloger,Description) VALUES(?,?,?,?)";

    if (finsert($str, $values)) {
        return true;

    } else {
        return false;
    }

}

//get cabinet by id
function getCabinetById($id){
    $query="SELECT * FROM cabinet WHERE Cabinet_ID=?";
    $result=fselect_id($query,$id);
    return $result->fetchAll();
}

//update cabinet

function updateCabinet($values){
    $query="UPDATE cabinet SET Station_ID=?, Name=?, Dataloger=?, Description=? WHERE Cabinet_ID=?";
    if(fupdate($query,$values)){
        return true;
    }else{
        return false;
    }
}
//delete cabinet


function deleteCabinet($id){
    if(fdelete("cabinet",'Cabinet_ID',$id))
    {
        return true;
    }else{
        return false;
    }

}
