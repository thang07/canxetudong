<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/29/2015
 * Time: 12:54 PM
 */

require_once 'functions.php';


function getListStation($page){
    $query=fselect("station","Station_ID",$page);
    $results=$query->fetchAll();
    return $results;
}

//insert station

function insertStation($value){

//insert station
   $str="INSERT INTO station(Location_ID,Name,Weight,Lat,Longs ,Owner,Description) values(?,?,?,?,?,?,?)";
    if(finsert($str,$value)){
        return true;
    }else
        return false;

}
//delete stration

function delStation($id){
    if(fdelete("station",'Station_ID',$id))
    {
        return true;
    }else{
        return false;
    }

}
//Get station by id
function getStationId($id){
    $query="SELECT * FROM station WHERE Station_ID=?";
    $result=fselect_id($query,$id);
    return $result->fetchAll();

}

//update station
function updateStation($values)
{
    $query="UPDATE station SET Location_ID=?, Name=?, Weight=?, Lat=?, Longs=?, Owner=?, Description=? WHERE Station_ID=?";
    if(fupdate($query,$values)){
        return true;
    }else{
        return false;
    }
}