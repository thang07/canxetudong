<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/29/2015
 * Time: 12:54 PM
 */

require "include/smarty.php";
require "include/station_func.php";
require "include/location_func.php";

//show list station
if (isset($_GET['list']) == "listStation") {
    $listStation = getListStation(10);
    $smarty->assign("listStation", $listStation);
    $smarty->assign("title", "Quản lý Trạm");
    $smarty->display('listStation.tpl');
}

//Show add station form
else if (isset($_GET['frmAdd']) == 'addStation') {
    $getLocation = getListLocation(100);
    $smarty->assign("listLocation", $getLocation);
    $smarty->assign("title", "Trạm");
    $smarty->display('addStation.tpl');
}

 //insert station  table
else if (isset($_REQUEST['action']) == "addStation") {
    $location_id = $_POST['location'];
    $name = $_POST['name'];
    $weigth = $_POST['weight'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];
    $owner=$_POST['owner'];
    $description=$_POST['description'];

    $values=array($location_id,$name,$weigth,$lat,$long,$owner,$description);
    $rs=insertStation($values);
    if($rs==true){
        $smarty->clearCache('listStation.tpl');
        header('location:station.php?list=listStation');
    }else{
        echo "<script language='javascript'>
            alert('Chưa insert, try again ');
            javascript:window.history.back(-1);
        </script>";
    }

}

//Delete station
else if(isset($_GET['delStation'])=='del'){
    $location_id=$_GET['id'];
    delStation($location_id);
}

//Show update form
else if(isset($_GET['show'])=='frmUpdate'){
    $getLocation = getListLocation(100);//get location
    $smarty->assign("listLocation", $getLocation);
    $getStationId = getStationId($_REQUEST['id']);//get station by id
    $smarty->assign("getStationId", $getStationId);
    $smarty->assign('title', 'Cập nhật Tram');
    $smarty->display('updateStation.tpl');
}
else if(isset($_REQUEST['actionUpdate'])=='updateStation'){

    $location_id = $_POST['location'];
    $station_id=$_POST['stationId'];
    $name = $_POST['name'];
    $weight = $_POST['weight'];
    $lat = $_POST['lat'];
    $long = $_POST['long'];
    $owner=$_POST['owner'];
    $description=$_POST['description'];

    $values=array($location_id,$name,$weight,$lat,$long,$owner,$description,$station_id);
    $rs=updateStation($values);
    if($rs==true){
        $smarty->assign('title', 'Danh Sách Trạm');
        $smarty->clearCache('listStation.tpl');
        header('location:station.php?list=listStation');
    }else{
        echo "<script language='javascript'>
            alert('Vui lòng nhập lại thông tin, try again ');
            javascript:window.history.back(-1);
        </script>";
    }


}