<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/26/2015
 * Time: 9:54 PM
 */
require "include/smarty.php";
require "include/users_func.php";


if (isset($_SESSION['Login'])) {
    header("location:index.php");
} else {
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $_username= trim($_POST['username']);
        $_password=sha1(($_POST['password']));
       // $_password=(($_POST['password']));
        if(loginUser($_username,$_password)){
            $smarty->assign("User_id",$_SESSION["User_id"]);
            $smarty->assign("Role",$_SESSION["Role"]);
            $smarty->assign("Email",$_SESSION["Email"]);
            $smarty->assign("Login",$_SESSION["Login"]);
            header("location:index.php");
        }
        else {
            $smarty->assign("title","Login Page");
            header("location:login.php");
        }
    }
    $smarty->assign("title","Login Page");
    $smarty->display("login.tpl");
}