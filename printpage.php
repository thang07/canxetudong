<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/28/2015
 * Time: 4:23 PM
 */

require "include/smarty.php";
require "include/printpage_func.php";


if (!isset($_SESSION['Login'])) {
    $smarty->assign("title", "Login page");
    header("location:login.php");//Chuyen huong trang login

}else if(isset($_GET['get'])=='print'){

    $smarty->assign("title", "Trang in");
    $smarty->display('printPage.tpl');
}