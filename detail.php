<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/28/2015
 * Time: 4:19 PM
 */

require "include/smarty.php";
require "include/detail_func.php";


  if(isset($_GET['vId'])) {

      $verhicelId=$_GET['vId'];
      $getVehicle=getVehicleDetailById($verhicelId);

      $getAxlesById=getAxlesById($verhicelId);
      $smarty->assign("getVehicle",$getVehicle);
      $smarty->assign("getAxle",$getAxlesById);
    $smarty->assign("title", "Trang chi tiết");
    $smarty->display('detail.tpl');
}