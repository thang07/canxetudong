<?php
/**
 * Created by PhpStorm.
 * User: bach
 * Date: 7/29/2015
 * Time: 4:20 PM
 */

require "include/smarty.php";
require "include/cabinet_func.php";
require "include/station_func.php";

//get list cabinet
if(isset($_GET['list'])=='listCabinet'){
    $getListCabinet=getListCabinet(10);
    $smarty->assign("listCabinet", $getListCabinet);
    $smarty->assign("title", "Quản lý Tủ");
    $smarty->display('listCabinet.tpl');

}
//show add cabinet form
else if(isset($_REQUEST['frmAdd'])=="addCabinet"){
    $listStation = getListStation(10);
    $smarty->assign("listStation", $listStation);
    $smarty->assign("title", "Thêm tủ");
    $smarty->display('addCabinet.tpl');

}
//insert cabinet
else if(isset($_REQUEST['action'])=='addCabinet'){
    $station_id = $_POST['station'];
    $name = $_POST['name'];
    $dataloger = $_POST['dataloger'];
    $description=$_POST['description'];
    $values=array($station_id,$name,$dataloger,$description);
    $rs=insertCabinet($values);
    if($rs==true){
        $smarty->clearCache('listCabinet.tpl');
        header('location:cabinet.php?list=listCabinet');
    }else{
        echo "<script language='javascript'>
            alert('Chưa insert, try again ');
            javascript:window.history.back(-1);
        </script>";
    }
}

//show update cabinet form
else if(isset($_GET['show'])=='frmUpdate'){
    $cabinet_id=$_GET['id'];
    $getCabinetById=getCabinetById($cabinet_id);
    $getStation=getListStation(100);
    $smarty->assign("getCabinetId", $getCabinetById);
    $smarty->assign("listStation", $getStation);
    $smarty->assign("title", "Cập nhật tủ");
    $smarty->display('updateCabinet.tpl');
}
else if(isset($_GET['frmUpdate'])=='frmUpdateCabinet'){
    $cabinet_id=$_POST['cabinetId'];
    $station_id = $_POST['station'];
    $name = $_POST['name'];
    $dataloger = $_POST['dataloger'];
    $description=$_POST['description'];
    $values=array($station_id,$name,$dataloger,$description,$cabinet_id);
    $rs=updateCabinet($values);
    if($rs==true){
        $smarty->assign('title', 'Danh Sách Tu');
        $smarty->clearCache('listCabenit.tpl');
        header('location:cabinet.php?list=listCabinet');
    }else{
        echo "<script language='javascript'>
            alert('Vui lòng nhập lại thông tin, try again ');
            javascript:window.history.back(-1);
        </script>";
    }

}
//delete cabinet
else if(isset($_GET['del'])=='del') {
   $cabinet_id = $_GET['id'];
    $rs = deleteCabinet($id);
    if ($rs==true) {
        $smarty->assign('title', 'Danh Sách Tủ');
        $smarty->clearCache('listCabenit.tpl');
        header('location:cabinet.php?list=listCabinet');
    }else{
        echo "<script language='javascript'>
            alert('Can not delete, try again ');
            javascript:window.history.back(-1);
        </script>";
    }
}


