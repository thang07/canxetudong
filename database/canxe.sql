-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2015 at 08:33 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `canxe`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditation`
--

DROP TABLE IF EXISTS `accreditation`;
CREATE TABLE IF NOT EXISTS `accreditation` (
  `Cccreditation_ID` bigint(20) DEFAULT NULL,
  `Plate_Number` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Weight` int(11) DEFAULT NULL,
  `Start_Time` datetime NOT NULL,
  `End_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `axles`
--

DROP TABLE IF EXISTS `axles`;
CREATE TABLE IF NOT EXISTS `axles` (
  `Axles_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Verhicle_ID` bigint(20) NOT NULL,
  `Axles_Base_ID` int(11) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `LeftWheelWeight` float DEFAULT NULL,
  `RigthWheelWeight` float DEFAULT NULL,
  `LeftRightImbalance` varchar(50) DEFAULT NULL,
  `Distance` float DEFAULT NULL,
  PRIMARY KEY (`Axles_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `axles`
--

INSERT INTO `axles` (`Axles_ID`, `Verhicle_ID`, `Axles_Base_ID`, `Weight`, `LeftWheelWeight`, `RigthWheelWeight`, `LeftRightImbalance`, `Distance`) VALUES
(1, 1, 1, 6000, 2500, 3500, '40/60', 0),
(2, 1, 1, 7000, 3500, 3500, '50/50', 1.3),
(3, 2, 1, 10000, 5000, 5000, '50/50', 0),
(4, 2, 2, 12000, 5500, 6500, '45/55', 1.2),
(5, 3, 2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `axlesbase`
--

DROP TABLE IF EXISTS `axlesbase`;
CREATE TABLE IF NOT EXISTS `axlesbase` (
  `Axles_Base_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `Axles_Group` int(11) DEFAULT NULL,
  `Distance_Min` int(11) DEFAULT NULL,
  `Distance_Max` int(11) DEFAULT NULL,
  `Discription` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Axles_Base_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `axlesbase`
--

INSERT INTO `axlesbase` (`Axles_Base_ID`, `Name`, `Weight`, `Axles_Group`, `Distance_Min`, `Distance_Max`, `Discription`) VALUES
(1, 'Trục đơn', 10, 1, 0, 0, NULL),
(2, 'Trục kép 2 ', 11, 2, 0, 99, 'd<1m'),
(3, 'Trục kép 2', 16, 2, 100, 129, '1 <=d < 1,3'),
(4, 'Trục kép 2 ', 18, 2, 130, 150, 'd>= 1,3'),
(5, 'Trục kép 3', 21, 3, 0, 130, 'd<=1,3'),
(6, 'Trục kép 3', 24, 3, 131, 150, 'd>1,3');

-- --------------------------------------------------------

--
-- Table structure for table `cabinet`
--

DROP TABLE IF EXISTS `cabinet`;
CREATE TABLE IF NOT EXISTS `cabinet` (
  `Cabinet_ID` int(18) NOT NULL AUTO_INCREMENT,
  `Station_ID` int(11) DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Dataloger` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Cabinet_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `cabinet`
--

INSERT INTO `cabinet` (`Cabinet_ID`, `Station_ID`, `Name`, `Dataloger`, `Description`) VALUES
(1, 1, 'Tủ 1', '123456789', 'Tủ quản lý 1 làn, cho thử nghiệm'),
(2, 1, 'Tủ 2', '987654321', 'Tủ trên làn 4,7'),
(3, 2, 'Tủ 1', '111111111', 'Tủ ở làn 2, trạm thu phí NVL'),
(4, 2, 'Tủ 2', '222222222', 'Tủ quản 2 làn 4,7 trạm NVL'),
(5, 3, 'Tủ 1', '333333333', 'Tủ quản lý 1 làn HN-HP'),
(6, 2, 'tu 55', 'adaa', 'ffsss');

-- --------------------------------------------------------

--
-- Table structure for table `crossing`
--

DROP TABLE IF EXISTS `crossing`;
CREATE TABLE IF NOT EXISTS `crossing` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CrossingID` varchar(50) DEFAULT NULL,
  `Lane_ID` int(11) DEFAULT NULL,
  `Plate_ID` int(11) DEFAULT NULL,
  `Version` varchar(50) DEFAULT NULL,
  `MetrologicalID` int(11) DEFAULT NULL,
  `StartTime` varchar(50) DEFAULT NULL,
  `LaneNo` varchar(50) DEFAULT NULL,
  `ErrorFlag` varchar(50) DEFAULT NULL,
  `WarningFlag` varchar(50) DEFAULT NULL,
  `ViolationFlag` varchar(50) DEFAULT NULL,
  `Direction` varchar(50) DEFAULT NULL,
  `FrontToFront` varchar(50) DEFAULT NULL,
  `BackToFront` varchar(50) DEFAULT NULL,
  `Duration` varchar(50) DEFAULT NULL,
  `VehicleLength` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `GrossWeight` varchar(50) NOT NULL,
  `LeftWeight` varchar(50) NOT NULL,
  `RightWeight` varchar(50) NOT NULL,
  `Velocity` varchar(50) NOT NULL,
  `WheelBase` varchar(50) NOT NULL,
  `AxlesCount` varchar(50) NOT NULL,
  `MassUnit` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `VelocityUnit` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `DistanceUnit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lane`
--

DROP TABLE IF EXISTS `lane`;
CREATE TABLE IF NOT EXISTS `lane` (
  `Lane_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cabinet_ID` int(18) DEFAULT NULL,
  `lane_Number` int(11) DEFAULT NULL,
  `Name` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sensor_Count` int(11) DEFAULT NULL,
  `Direction` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Lane_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lane`
--

INSERT INTO `lane` (`Lane_ID`, `Cabinet_ID`, `lane_Number`, `Name`, `Sensor_Count`, `Direction`, `Description`) VALUES
(1, 1, 6, 'Làn o6 tô tải, 1 dừng', 4, 'An Sương - Long An', NULL),
(2, 2, 3, 'Làn tự động', 2, 'An Sương - Long An', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `Location_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lat` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Long` varchar(10) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `City_Code` int(10) DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Location_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`Location_ID`, `Name`, `Lat`, `Long`, `City_Code`, `Description`) VALUES
(1, 'TP.HCM', '10.773510', '106.693848', NULL, 'Thành Phố Hồ Chí Minh, Việt Nam'),
(2, 'Hà Nội', '21.032230', '105.835419', NULL, 'Thủ đô Hà Nội, Việt Nam');

-- --------------------------------------------------------

--
-- Table structure for table `plate`
--

DROP TABLE IF EXISTS `plate`;
CREATE TABLE IF NOT EXISTS `plate` (
  `Plate_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Time` datetime DEFAULT NULL,
  `Plate_Name` varchar(50) DEFAULT NULL,
  `Plate_Number` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Plate_Image` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vehicle_Image` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Plate_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `plate`
--

INSERT INTO `plate` (`Plate_ID`, `Time`, `Plate_Name`, `Plate_Number`, `Plate_Image`, `Vehicle_Image`, `Description`) VALUES
(1, NULL, '50P-1234', NULL, NULL, '2truc.jpg', NULL),
(2, NULL, '51A-2345', NULL, NULL, '14232000664858139510.jpg', NULL),
(3, NULL, '52E-1234', NULL, NULL, '14232000664858139510.jpg', NULL),
(5, NULL, '78B-02312', NULL, NULL, '14232000664858139510.jpg', NULL),
(6, NULL, '98M-03056', NULL, NULL, '14232000664858139510.jpg', NULL),
(7, NULL, '47L-6696', NULL, NULL, '14232000664858139510.jpg', NULL),
(8, NULL, '47N2-7473', NULL, NULL, '14232000664858139510.jpg', NULL),
(9, NULL, '47C-0145', NULL, NULL, '14232000664858139510.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `MaxSpeed` int(11) DEFAULT NULL,
  `MinSpeed` int(11) DEFAULT NULL,
  `MaxWeight` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

DROP TABLE IF EXISTS `station`;
CREATE TABLE IF NOT EXISTS `station` (
  `Station_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Location_ID` int(11) DEFAULT NULL,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Weight` int(11) DEFAULT NULL,
  `Lat` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Longs` varchar(10) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `Owner` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Station_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`Station_ID`, `Location_ID`, `Name`, `Weight`, `Lat`, `Longs`, `Owner`, `Description`) VALUES
(1, 1, 'An Sương An Lạc', 80, '10.801723', '106.598007', 'IDICO', 'Trạm thu phí ASAL do IDICO làm chủ đầu tư'),
(2, 1, 'Nguyễn Văn Linh', 90, '10.727647', '106.671921', 'Trung tâm Hầm', 'Trạm cân NGV đặt tại cầu ông Lớn'),
(3, 2, 'Cao tốc HN - HP', 50, '20.812491', '106.500167', 'Tassco', 'Trạm cân ở KM50, cao tốc HN-HP');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) CHARACTER SET latin1 NOT NULL,
  `password` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(5) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `name`, `role`) VALUES
(3, 'bich', '00e5b06045c0f3266fa4cb5e26a3ec9a04f2ff30', 'hongdiepbach@mail.com', 'Hong diep', 'admin'),
(5, 'admin', '00e5b06045c0f3266fa4cb5e26a3ec9a04f2ff30', 'admin@gmail.com.vn', 'admin', 'admin'),
(6, 'mod', '00e5b06045c0f3266fa4cb5e26a3ec9a04f2ff30', 'mod@gmail.com', 'mod', 'mod');

-- --------------------------------------------------------

--
-- Table structure for table `verhicle`
--

DROP TABLE IF EXISTS `verhicle`;
CREATE TABLE IF NOT EXISTS `verhicle` (
  `Verhicle_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Lane_ID` int(11) DEFAULT NULL,
  `Plate_ID` bigint(20) DEFAULT NULL,
  `Verhicle_Base_ID` int(11) DEFAULT NULL,
  `Time` datetime DEFAULT CURRENT_TIMESTAMP,
  `Lenght` float DEFAULT NULL,
  `Left_Weight` float DEFAULT NULL,
  `Right_Weight` float DEFAULT NULL,
  `Gross_Weight` float DEFAULT NULL,
  `Velocity` float DEFAULT NULL,
  `Axles_Count` int(11) DEFAULT NULL,
  PRIMARY KEY (`Verhicle_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `verhicle`
--

INSERT INTO `verhicle` (`Verhicle_ID`, `Lane_ID`, `Plate_ID`, `Verhicle_Base_ID`, `Time`, `Lenght`, `Left_Weight`, `Right_Weight`, `Gross_Weight`, `Velocity`, `Axles_Count`) VALUES
(1, 1, 1, 1, '2015-07-11 00:00:00', 7, 8000, 8000, 16000, 25, 2),
(2, 2, 2, 2, '2015-07-22 00:00:00', 9.2, 13000, 14000, 27000, 30, 3),
(3, 2, 3, 3, NULL, 13.4, 14500, 14500, 29000, 10, 4),
(4, 2, 4, 4, '2015-07-15 00:00:00', 15, 18000, 22000, 40000, 15, 5),
(5, 1, 5, 5, NULL, 8, 8000, 12000, 20000, 16, 3),
(6, 1, 6, 6, '2015-07-30 03:07:06', 11, 25000, 29000, 54000, 21, 4),
(7, 2, 7, 7, NULL, 12, 20000, 20000, 40000, 7, 5),
(8, 1, 8, 8, '2015-07-30 11:08:00', 17, NULL, NULL, 60000, 45, 6);

-- --------------------------------------------------------

--
-- Table structure for table `verhiclebase`
--

DROP TABLE IF EXISTS `verhiclebase`;
CREATE TABLE IF NOT EXISTS `verhiclebase` (
  `Verhicle_Base_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `Axles_Count` int(11) DEFAULT NULL,
  `Height` float DEFAULT NULL,
  `Width` float DEFAULT NULL,
  `Lenght` float DEFAULT NULL,
  `Description` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Verhicle_Base_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `verhiclebase`
--

INSERT INTO `verhiclebase` (`Verhicle_Base_ID`, `Name`, `Weight`, `Axles_Count`, `Height`, `Width`, `Lenght`, `Description`) VALUES
(1, 'Xe thân liền 2 trục', 16, 2, NULL, NULL, NULL, NULL),
(2, 'Xe thân liền 3 trục', 24, 3, NULL, NULL, NULL, NULL),
(3, 'Xe thân liền 4 trục', 30, 4, NULL, NULL, NULL, NULL),
(4, 'Xe thân liền từ 5 trục trở lên', 34, 5, NULL, NULL, NULL, NULL),
(5, 'Xe đầu kéo và rơ moóc 3 trục', 26, 3, NULL, NULL, NULL, NULL),
(6, 'Xe đầu kéo và rơ moóc 4 trục', 34, 4, NULL, NULL, NULL, NULL),
(7, 'Xe đầu kéo và rơ moóc 5 trục', 44, 5, NULL, NULL, NULL, NULL),
(8, 'Xe đầu kéo và rơ moóc  từ 6 trục', 48, 6, NULL, NULL, NULL, NULL);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
